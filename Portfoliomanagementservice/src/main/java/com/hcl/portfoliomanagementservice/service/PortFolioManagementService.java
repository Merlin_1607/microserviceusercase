package com.hcl.portfoliomanagementservice.service;

import com.hcl.portfoliomanagementservice.dto.PortfolioDto;
import com.hcl.portfoliomanagementservice.dto.PortfolioFinalDto;
import com.hcl.portfoliomanagementservice.dto.TransactionDto;

public interface PortfolioManagementService {
	public PortfolioFinalDto getPortfolioDetails(int accountId);

	public boolean placeOrdersByPortfolio(TransactionDto transactionDto);

	public PortfolioDto getPortfolios(int portfolioId);

}
