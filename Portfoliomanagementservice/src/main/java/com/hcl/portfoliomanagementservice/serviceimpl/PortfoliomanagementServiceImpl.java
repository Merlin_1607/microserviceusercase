package com.hcl.portfoliomanagementservice.serviceimpl;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.hcl.portfoliomanagementservice.controller.UnitCostConnect;
import com.hcl.portfoliomanagementservice.dto.PortfolioDto;
import com.hcl.portfoliomanagementservice.dto.PortfolioFinalDto;
import com.hcl.portfoliomanagementservice.dto.PortfolioRequestDto;
import com.hcl.portfoliomanagementservice.dto.TransactionDto;
import com.hcl.portfoliomanagementservice.exception.AccountNotFoundException;
import com.hcl.portfoliomanagementservice.exception.InvalidPortFolioIdException;
import com.hcl.portfoliomanagementservice.exception.NoUserIdFoundException;
import com.hcl.portfoliomanagementservice.model.Account;
import com.hcl.portfoliomanagementservice.model.Portfolio;
import com.hcl.portfoliomanagementservice.model.Transaction;
import com.hcl.portfoliomanagementservice.model.User;
import com.hcl.portfoliomanagementservice.repository.AccountRepository;
import com.hcl.portfoliomanagementservice.repository.PortfolioManagementServiceRepository;
import com.hcl.portfoliomanagementservice.repository.TransactionRepository;
import com.hcl.portfoliomanagementservice.repository.UserRepository;
import com.hcl.portfoliomanagementservice.service.PortfolioManagementService;

@Service
@Transactional
public class PortfolioManagementServiceImpl implements PortfolioManagementService {

	// Configure loggers
	Logger logger = LoggerFactory.getLogger(PortfolioManagementServiceImpl.class);

	@Autowired
	PortfolioManagementServiceRepository portfolioRepo;
	@Autowired
	TransactionRepository transactionRepo;
	@Autowired
	UnitCostConnect unitCostConnect;

	@Autowired
	AccountRepository accountRepo;

	@Autowired
	UserRepository userRepo;

	public PortfolioFinalDto getPortfolioDetails(int accountId) {

		logger.trace("Get all Portfolio Method is called");
		List<PortfolioRequestDto> portfolioDtoList = transactionRepo.findByAccountId(accountId).stream().map(e -> {
			PortfolioRequestDto portfolioRequestDto = new PortfolioRequestDto();
			portfolioRequestDto.setPortfolioName(e.getPortfolio().getPortfolioName());
			portfolioRequestDto.setDateOfPurchase(e.getDate());
			portfolioRequestDto.setQuantity(e.getQuantity());
			portfolioRequestDto.setTotalcost(e.getTotalcost());
			return portfolioRequestDto;

		}).collect(Collectors.toList());
		if (portfolioDtoList.isEmpty()) {
			throw new AccountNotFoundException("No portfolio purchased from this account");
		}
		logger.trace("Portfoliorequestdto " + portfolioDtoList);
		PortfolioFinalDto portfoliofinal = new PortfolioFinalDto();
		portfoliofinal.setPortfoliolist(portfolioDtoList);
		portfoliofinal.setDate(LocalDate.now());

		double total = 0;
		for (PortfolioRequestDto price : portfolioDtoList) {
			total += price.getTotalcost();

		}
		portfoliofinal.setGrandtotal(total);
		logger.trace("Portfoliofinaldto " + portfoliofinal);

		return portfoliofinal;
	}

	public boolean placeOrdersByPortfolio(TransactionDto transactionDto) {
		logger.trace("To place order method is called");
		Transaction transaction = new Transaction();
		transaction.setDate(LocalDate.now());
		transaction.setQuantity(transactionDto.getQuantity());
		Account account = accountRepo.findById(transactionDto.getAccountId()).orElseThrow(
				() -> new AccountNotFoundException("No Account Found with this Id.....Create new Account"));
		User user = userRepo.findById(transactionDto.getUserId())
				.orElseThrow(() -> new NoUserIdFoundException("No user exist with this userid"));
		Portfolio portfolio = portfolioRepo.findById(transactionDto.getPortfolioId())
				.orElseThrow(() -> new InvalidPortFolioIdException("No portfolio Id exist"));
		transaction.setAccount(account);
		transaction.setPortfolio(portfolio);
		transaction.setUser(user);
		double price = unitCostConnect.getUnitCost(portfolio.getPortfolioName());
		logger.info("Unit Cost : " + price);
		transaction.setTotalcost(transactionDto.getQuantity() * price);
		Transaction tansactionPersist = transactionRepo.save(transaction);
		if (ObjectUtils.isEmpty(tansactionPersist))
			return false;

		return true;

	}

	@Override
	public PortfolioDto getPortfolios(int portfolioId) {
		logger.trace("Get all Portfolio Method is called");
		Transaction transaction = transactionRepo.findByPortfolioId(portfolioId);
		if (!ObjectUtils.isEmpty(transaction)) {
			PortfolioDto portfolioDto = new PortfolioDto();

			portfolioDto.setPortfolioName(transaction.getPortfolio().getPortfolioName());
			portfolioDto.setDateOfPurchase(transaction.getDate());
			portfolioDto.setCostOfPurchase(transaction.getTotalcost());
			portfolioDto.setQuantity(transaction.getQuantity());
			double price = unitCostConnect.getUnitCost(transaction.getPortfolio().getPortfolioName());
			logger.trace("Unit Cost" + price);
			portfolioDto.setCurrentValue(transaction.getQuantity() * price);
			return portfolioDto;

		}
		throw new InvalidPortFolioIdException("No Potfolio details Found");

	}
}
