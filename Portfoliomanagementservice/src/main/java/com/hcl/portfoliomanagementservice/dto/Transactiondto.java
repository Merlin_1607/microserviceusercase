package com.hcl.portfoliomanagementservice.dto;

import javax.validation.Valid;
import javax.validation.constraints.Min;

public class TransactionDto {
	
   
	@Valid @Min(1)
	private int portfolioId;
	@Valid @Min(1)
	private int accountId;
	@Valid @Min(1)
	private int userId;
	@Valid @Min(1)
	private int quantity;
	
	
	@Override
	public String toString() {
		return "Transactiondto [portfolioId=" + portfolioId + ", accountId=" + accountId + ", userId=" + userId
				+ ", quantity=" + quantity + "]";
	}
	public int getPortfolioId() {
		return portfolioId;
	}
	public void setPortfolioId(int portfolioId) {
		this.portfolioId = portfolioId;
	}
	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}
