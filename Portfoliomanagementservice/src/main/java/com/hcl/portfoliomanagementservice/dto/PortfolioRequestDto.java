package com.hcl.portfoliomanagementservice.dto;

import java.time.LocalDate;
import java.util.List;

public class PortfolioRequestDto {
	
	private String portfolioName;
	private int quantity;
	private double totalcost;
	private LocalDate dateOfPurchase;
	public String getPortfolioName() {
		return portfolioName;
	}
	public void setPortfolioName(String portfolioName) {
		this.portfolioName = portfolioName;
	}
	public int getQuantity() {
		return quantity;
	}
	@Override
	public String toString() {
		return "PortfolioRequestDto [portfolioName=" + portfolioName + ", quantity=" + quantity + ", totalcost="
				+ totalcost + ", dateOfPurchase=" + dateOfPurchase + "]";
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getTotalcost() {
		return totalcost;
	}
	public void setTotalcost(double totalcost) {
		this.totalcost = totalcost;
	}
	public LocalDate getDateOfPurchase() {
		return dateOfPurchase;
	}
	public void setDateOfPurchase(LocalDate dateOfPurchase) {
		this.dateOfPurchase = dateOfPurchase;
	}

}
