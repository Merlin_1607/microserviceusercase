package com.hcl.portfoliomanagementservice.dto;

import java.time.LocalDate;
import java.util.Date;


import com.hcl.portfoliomanagementservice.model.Portfolio;


public class PortfolioDto {
	private String portfolioName;
	
	private int quantity;
	private double costOfPurchase;
	private double currentValue;
	private LocalDate dateOfPurchase;
	
	
	
	public String getPortfolioName() {
		return portfolioName;
	}
	public void setPortfolioName(String portfolioName) {
		this.portfolioName = portfolioName;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getCostOfPurchase() {
		return costOfPurchase;
	}
	public void setCostOfPurchase(double costOfPurchase) {
		this.costOfPurchase = costOfPurchase;
	}
	public double getCurrentValue() {
		return currentValue;
	}
	public void setCurrentValue(double currentValue) {
		this.currentValue = currentValue;
	}
	public LocalDate getDateOfPurchase() {
		return dateOfPurchase;
	}
	public void setDateOfPurchase(LocalDate dateOfPurchase) {
		this.dateOfPurchase = dateOfPurchase;
	}
	@Override
	public String toString() {
		return "Portfoliodto [portfolioName=" + portfolioName + ", quantity=" + quantity + ", costOfPurchase="
				+ costOfPurchase + ", currentValue=" + currentValue + ", dateOfPurchase=" + dateOfPurchase + "]";
	}
	
	
}
