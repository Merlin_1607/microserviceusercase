package com.hcl.portfoliomanagementservice.dto;

import java.time.LocalDate;
import java.util.List;

public class PortfolioFinalDto {
	 private List<PortfolioRequestDto> portfoliolist;
	 private double grandtotal;
	 private LocalDate date;
	public List<PortfolioRequestDto> getPortfoliolist() {
		return portfoliolist;
	}
	public void setPortfoliolist(List<PortfolioRequestDto> portfoliolist) {
		this.portfoliolist = portfoliolist;
	}
	public double getGrandtotal() {
		return grandtotal;
	}
	public void setGrandtotal(double grandtotal) {
		this.grandtotal = grandtotal;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "PortfoliofinalDto [portfoliolist=" + portfoliolist + ", grandtotal=" + grandtotal + ", date=" + date
				+ "]";
	}
	
	
	 

}
