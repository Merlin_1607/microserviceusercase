package com.hcl.portfoliomanagementservice.exception;

public class AccountNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String message;


	public AccountNotFoundException() {
		super();
	}


	public AccountNotFoundException(String message) {
		super(message);
		this.message = message;
	}
	
	

}
