package com.hcl.portfoliomanagementservice.exception;

public class NoUserIdFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String message;
	
	

	public NoUserIdFoundException() {
		super();
	}



	public NoUserIdFoundException(String message) {
		super(message);
		this.message = message;
	}
	
	

}
