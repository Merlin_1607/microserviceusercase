package com.hcl.portfoliomanagementservice.exception;



import java.time.LocalDateTime;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.hcl.portfoliomanagementservice.ApiStatusCode;


import feign.FeignException;






@RestControllerAdvice //=  @ControllerAdvice + @ResponseBody
public class GlobalExceptionHandler {

	
	
	
	@ExceptionHandler(InvalidPortFolioIdException.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(InvalidPortFolioIdException ex) {
	ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatuscode(ApiStatusCode.PORTFOLIOID_NOT_FOUND);
		errorResponse.setDateTime(LocalDateTime.now());
		
		return new ResponseEntity<ErrorResponse>(errorResponse,HttpStatus.OK);
	}	
	@ExceptionHandler(AccountNotFoundException.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(AccountNotFoundException ex) {
	ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatuscode(ApiStatusCode.ACCOUNT_NOT_FOUND);
		errorResponse.setDateTime(LocalDateTime.now());
		
		return new ResponseEntity<ErrorResponse>(errorResponse,HttpStatus.OK);
	}	
	@ExceptionHandler(NoUserIdFoundException.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(NoUserIdFoundException ex) {
	ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatuscode(ApiStatusCode.USER_NOT_FOUND);
		errorResponse.setDateTime(LocalDateTime.now());
		
		return new ResponseEntity<ErrorResponse>(errorResponse,HttpStatus.OK);
	}	
	
	@ExceptionHandler(FeignException.class)
	public ResponseEntity<ErrorResponse> FeignExceptionHandler(FeignException ex) {
	ErrorResponse errorResponse = new ErrorResponse();
	errorResponse.setMessage(ex.getMessage());
	errorResponse.setStatuscode(ApiStatusCode.INVALID_DATA);
	errorResponse.setDateTime(LocalDateTime.now());
	return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.OK);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(MethodArgumentNotValidException ex) {
		
		ValidationErrorResponse errorResponse = new ValidationErrorResponse();
		errorResponse.setMessage("Input Data is Invalid");
		errorResponse.setDateTime(LocalDateTime.now());
		errorResponse.setStatuscode(400);
		
		List<FieldError> errors = ex.getBindingResult().getFieldErrors();
		errors.forEach(error -> {
			errorResponse.getErrorsMap().put(error.getField(), error.getDefaultMessage());
		});
		
		return new ResponseEntity<ErrorResponse>(errorResponse,HttpStatus.BAD_REQUEST);
	}
	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(ConstraintViolationException ex) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatuscode(400);
		errorResponse.setDateTime(LocalDateTime.now());
		
		return new ResponseEntity<ErrorResponse>(errorResponse,HttpStatus.OK);
	}
	
	
}