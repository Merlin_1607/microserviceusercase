package com.hcl.portfoliomanagementservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hcl.portfoliomanagementservice.model.Portfolio;

@Repository
public interface PortfolioManagementServiceRepository extends JpaRepository<Portfolio, Integer> {

	@Query(value = "Select * from Portfolio where account_account_id =?1", nativeQuery = true)
	List<Portfolio> findAllPortfolios(int accountId);

}
