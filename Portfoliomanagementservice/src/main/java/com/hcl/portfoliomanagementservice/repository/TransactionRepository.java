package com.hcl.portfoliomanagementservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hcl.portfoliomanagementservice.model.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
	@Query(value = "Select * from Transaction where portfolio_id = ?1", nativeQuery = true)
	public Transaction findByPortfolioId(int portfolioId);

	@Query(value = "Select * from Transaction where account_id = ?1", nativeQuery = true)
	public List<Transaction> findByAccountId(int accountId);

}
