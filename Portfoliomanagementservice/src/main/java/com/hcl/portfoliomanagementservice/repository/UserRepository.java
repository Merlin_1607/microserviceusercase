package com.hcl.portfoliomanagementservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hcl.portfoliomanagementservice.model.User;




@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

//	public User findByUserNameAndPassword(String username, String password);
//
//	 public User findByUserName(Credentials credentials);
//
//	public User findByUserName(String username);
//	public User findByuserId(int userId);
//	
}
