package com.hcl.portfoliomanagementservice.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;



//Portfolio Entity Table

@Entity
public class Portfolio {
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private int portfolioId;
	private String portfolioName;
	
	@ManyToOne
	private Account account;
	
	public Portfolio(int portfolioId, String portfolioName, double costofPurchase,
			Account account) {
		super();
		this.portfolioId = portfolioId;
		this.portfolioName = portfolioName;
		
		this.account = account;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	public Portfolio() {
		super();
	}
	public Portfolio(int portfolioId, String portfolioName, double costofPurchase) {
		super();
		this.portfolioId = portfolioId;
		this.portfolioName = portfolioName;
		
	}
	public int getPortfolioId() {
		return portfolioId;
	}
	public void setPortfolioId(int portfolioId) {
		this.portfolioId = portfolioId;
	}
	public String getPortfolioName() {
		return portfolioName;
	}
	public void setPortfolioName(String portfolioName) {
		this.portfolioName = portfolioName;
	}
	@Override
	public String toString() {
		return "Portfolio [portfolioId=" + portfolioId + ", portfolioName=" + portfolioName + ", account=" + account
				+ "]";
	}
	
}
