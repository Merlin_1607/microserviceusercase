package com.hcl.portfoliomanagementservice.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.portfoliomanagementservice.dto.PortfolioDto;
import com.hcl.portfoliomanagementservice.dto.PortfolioFinalDto;
import com.hcl.portfoliomanagementservice.dto.TransactionDto;

import com.hcl.portfoliomanagementservice.service.PortfolioManagementService;

@RestController
public class PortfolioManagementServiceController {

	// Configure loggers
	Logger logger = LoggerFactory.getLogger(PortfolioManagementServiceController.class);

	@Autowired
	PortfolioManagementService portfolioService;
	@Autowired
	UnitCostConnect unitCostConnect;
	
	
	

	@GetMapping("/portfolios/{accountId}")
	public PortfolioFinalDto getPortfolios(@PathVariable("accountId") int accountId) {

		logger.trace("Get portfolio method is called");
		return portfolioService.getPortfolioDetails(accountId);

	}

	@PostMapping("/purchase")
	public ResponseEntity<String> placeOrderByPortfolio(@Valid @RequestBody TransactionDto transactionDto) {

		logger.trace("Place Order method is called");
		if (portfolioService.placeOrdersByPortfolio(transactionDto)) {
			return new ResponseEntity<>("Portfolio ordered successfully", HttpStatus.OK);
		}
		return new ResponseEntity<>("Portfolio not ordered", HttpStatus.BAD_REQUEST);

	}

	@GetMapping("/portfoliodetails/{portfolioId}")
	public PortfolioDto getPortfolioDetails(@PathVariable("portfolioId") int portfolioId) {
		logger.trace("Get details of portfolio method is called");
		return portfolioService.getPortfolios(portfolioId);
	}
	
	

}
