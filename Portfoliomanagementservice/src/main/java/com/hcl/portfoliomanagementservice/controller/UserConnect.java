package com.hcl.portfoliomanagementservice.controller;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.hcl.userservice.dto.Credentials;
import com.hcl.userservice.model.Account;



@FeignClient(name="userservice")
public interface UserConnect {
	@GetMapping("/accounts/{userId}")
	public List<Account> getaccounts( int userId);
	@PostMapping("/users/login")
	public ResponseEntity<String> login( Credentials credentials) ;
		
		
	

}
