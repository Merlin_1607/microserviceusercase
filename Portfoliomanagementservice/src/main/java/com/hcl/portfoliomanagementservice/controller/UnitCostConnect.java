package com.hcl.portfoliomanagementservice.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "Portfolioservice", url = "http://localhost:8083/")
public interface UnitCostConnect {
	
	
	@GetMapping("/portfolio/{portfolioName}")
	public double getUnitCost(@PathVariable String portfolioName);

}
