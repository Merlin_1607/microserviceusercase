package com.hcl.portfoliomanagementservice;
public class ApiStatusCode {

	public static final int PORTFOLIOID_NOT_FOUND= 600;
	public static final int INVALID_DATA = 400;
	public static final int ACCOUNT_NOT_FOUND = 500;
	public static final int USER_NOT_FOUND = 501;
}