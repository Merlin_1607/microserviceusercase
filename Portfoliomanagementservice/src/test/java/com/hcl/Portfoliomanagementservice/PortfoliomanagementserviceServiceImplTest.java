package com.hcl.Portfoliomanagementservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import com.hcl.portfoliomanagementservice.dto.PortfolioRequestDto;
import com.hcl.portfoliomanagementservice.dto.PortfolioDto;
import com.hcl.portfoliomanagementservice.dto.PortfolioFinalDto;
import com.hcl.portfoliomanagementservice.dto.TransactionDto;
import com.hcl.portfoliomanagementservice.exception.AccountNotFoundException;
import com.hcl.portfoliomanagementservice.model.Account;
import com.hcl.portfoliomanagementservice.model.Portfolio;
import com.hcl.portfoliomanagementservice.model.Transaction;
import com.hcl.portfoliomanagementservice.model.User;
import com.hcl.portfoliomanagementservice.repository.TransactionRepository;
import com.hcl.portfoliomanagementservice.serviceimpl.PortfolioManagementServiceImpl;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class PortfoliomanagementserviceServiceImplTest {
	@Mock
	TransactionRepository transactionRepo;

	@InjectMocks
	PortfolioManagementServiceImpl portfolioService;

	static Portfolio portfolio;
	static User user;
	static Account account;
	static Transaction transaction;
	static TransactionDto transactiondto;
	static List<Transaction> portfolioFinal;
	static List<PortfolioRequestDto> portfolioRequestlist;
	static List<PortfolioFinalDto> portfolioFinalDto;
	static PortfolioRequestDto portfolioRequestOne;
	static PortfolioDto portfolios;

	@BeforeAll
	public static void setup() {

		account = new Account();
		account.setAccountId(123);
		portfolio = new Portfolio();
		portfolio.setPortfolioName("Apple");
		portfolio.setPortfolioId(1);
		transaction = new Transaction();
		transaction.setAccount(account);
		transaction.setUser(user);
		transaction.setPortfolio(portfolio);
		transaction.setQuantity(3);
		transaction.setDate(LocalDate.now());
		transaction.setTotalcost(300.00);

		PortfolioRequestDto portfoliorequestdto = new PortfolioRequestDto();
		portfoliorequestdto.setPortfolioName("Apple");
		portfoliorequestdto.setDateOfPurchase(LocalDate.now());
		portfoliorequestdto.setQuantity(2);
		portfoliorequestdto.setTotalcost(456.00);
		portfolioRequestlist = new ArrayList<>();
		portfolioRequestlist.add(portfoliorequestdto);

		PortfolioFinalDto portfoliofinal = new PortfolioFinalDto();
		portfoliofinal.setPortfoliolist(portfolioRequestlist);
		portfoliofinal.setDate(LocalDate.now());
		portfoliofinal.setGrandtotal(300.00);

		portfolioFinal = new ArrayList<>();
		portfolioFinal.add(transaction);

		portfolios = new PortfolioDto();
		portfolios.setPortfolioName("Apple");
		portfolios.setQuantity(2);
		portfolios.setDateOfPurchase(LocalDate.now());
		portfolios.setCostOfPurchase(300.00);
		portfolios.setCurrentValue(876.00);

	}

	@Test
	@DisplayName("Get PortfolioDetails: Positive Function")
	public void getportfoliodetailsTest() {
		// context
		when(transactionRepo.findByAccountId(123)).thenReturn(portfolioFinal);
		// event
		PortfolioFinalDto portfoliofinalDto = portfolioService.getPortfolioDetails(123);
		// outcome
		assertEquals(portfolioFinal.size(), portfoliofinalDto.getPortfoliolist().size());

	}

	@Test
	@DisplayName("Get PortfolioDetails: Negative Function")
	public void getportfoliodetailsnegativeTest() {
		// context
		when(transactionRepo.findByAccountId(123)).thenReturn(portfolioFinal);

		// outcome
		assertThrows(AccountNotFoundException.class, () -> portfolioService.getPortfolioDetails(1232));

	}

//	@Test
//	@DisplayName("Get PortfolioDetails Based on portfolio Id: Positive Function")
//	public void getportfoliodetailsIdTest() {
//		// context
//		when(transactionRepo.save(any(Transaction.class))).thenReturn(transaction);
//		// event
//		boolean result = portfolioService.placeordersByPortfolio(transactiondto);
//		// outcome
//		assertTrue(result);
//
//	}

}
