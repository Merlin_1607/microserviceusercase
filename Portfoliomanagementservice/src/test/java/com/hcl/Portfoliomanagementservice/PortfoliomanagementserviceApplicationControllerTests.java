package com.hcl.Portfoliomanagementservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.hcl.portfoliomanagementservice.controller.PortfolioManagementServiceController;
import com.hcl.portfoliomanagementservice.dto.PortfolioRequestDto;
import com.hcl.portfoliomanagementservice.dto.PortfolioDto;
import com.hcl.portfoliomanagementservice.dto.PortfolioFinalDto;
import com.hcl.portfoliomanagementservice.dto.TransactionDto;
import com.hcl.portfoliomanagementservice.model.Account;
import com.hcl.portfoliomanagementservice.model.Portfolio;
import com.hcl.portfoliomanagementservice.model.Transaction;
import com.hcl.portfoliomanagementservice.model.User;
import com.hcl.portfoliomanagementservice.serviceimpl.PortfolioManagementServiceImpl;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness=Strictness.LENIENT)
class PortfoliomanagementserviceApplicationControllerTests {

	@Mock
	PortfolioManagementServiceImpl portfolioService;
	
	@InjectMocks
	PortfolioManagementServiceController portfolioController;
	
	static Portfolio portfolio;
	static User user;
	static Account account;
	static Transaction transaction;
	static TransactionDto transactiondto;
	static List<Transaction> portfolioFinal;
	static List<PortfolioRequestDto> portfolioRequestlist;
	static List<PortfolioFinalDto> portfolioFinalDto;
	static PortfolioRequestDto portfolioRequestOne;
	static PortfolioDto portfolios;
	static PortfolioFinalDto portfoliofinal;

	@BeforeAll
	public static void setup() {
		account = new Account();
		account.setAccountId(123);
		portfolio = new Portfolio();
		portfolio.setPortfolioName("Apple");
		portfolio.setPortfolioId(1);
		transaction = new Transaction();
		transaction.setAccount(account);
		transaction.setUser(user);
		transaction.setPortfolio(portfolio);
		transaction.setQuantity(3);
		transaction.setDate(LocalDate.now());
		transaction.setTotalcost(300.00);

		PortfolioRequestDto portfoliorequestdto = new PortfolioRequestDto();
		portfoliorequestdto.setPortfolioName("Apple");
		portfoliorequestdto.setDateOfPurchase(LocalDate.now());
		portfoliorequestdto.setQuantity(2);
		portfoliorequestdto.setTotalcost(456.00);
		portfolioRequestlist = new ArrayList<>();
		portfolioRequestlist.add(portfoliorequestdto);

		 portfoliofinal = new PortfolioFinalDto();
		portfoliofinal.setPortfoliolist(portfolioRequestlist);
		portfoliofinal.setDate(LocalDate.now());
		portfoliofinal.setGrandtotal(300.00);

		portfolioFinal = new ArrayList<>();
		portfolioFinal.add(transaction);

		portfolios = new PortfolioDto();
		portfolios.setPortfolioName("Apple");
		portfolios.setQuantity(2);
		portfolios.setDateOfPurchase(LocalDate.now());
		portfolios.setCostOfPurchase(300.00);
		portfolios.setCurrentValue(876.00);

	}
	
	@Test
	@DisplayName("Portfolio: Positive Function")
	public void getportfoliodetails() {
		
		//context
		when(portfolioService.getPortfolios(1)).thenReturn(portfolios);
		
		//event
		PortfolioDto portfolio=portfolioController.getPortfolioDetails(1);
		
		//outcome
		assertEquals(portfolios,portfolio);
}
	@Test
	@DisplayName("Portfolio Quantity: Positive Function")
	public void getportfoliodetailsTest() {
		
		//context
		when(portfolioService.getPortfolios(1)).thenReturn(portfolios);
		
		//event
		PortfolioDto portfolio=portfolioController.getPortfolioDetails(1);
		
		//outcome
		assertEquals(portfolio.getQuantity(),portfolios.getQuantity());
}
	
	@Test
	@DisplayName("PortfolioName: Positive Function")
	public void getportfoliodetailsNameTest() {
		
		//context
		when(portfolioService.getPortfolios(1)).thenReturn(portfolios);
		
		//event
		PortfolioDto portfolio=portfolioController.getPortfolioDetails(1);
		
		//outcome
		assertEquals(portfolio.getPortfolioName(),portfolios.getPortfolioName());
}
	@Test
	@DisplayName("PortfolioCostOfPurchase: Positive Function")
	public void getportfoliodetailsCostTest() {
		
		//context
		when(portfolioService.getPortfolios(1)).thenReturn(portfolios);
		
		//event
		PortfolioDto portfolio=portfolioController.getPortfolioDetails(1);
		
		//outcome
		assertEquals(portfolio.getCostOfPurchase(),portfolios.getCostOfPurchase());
}
	@Test
	@DisplayName("PortfolioCurrevtValue: Positive Function")
	public void getportfoliodetailsCurrentValueTest() {
		
		//context
		when(portfolioService.getPortfolios(1)).thenReturn(portfolios);
		
		//event
		PortfolioDto portfolio=portfolioController.getPortfolioDetails(1);
		
		//outcome
		assertEquals(portfolio.getCurrentValue(),portfolios.getCurrentValue());
}
	
	
	
	
	@Test
	@DisplayName("Portfolio Baseon AccountId:Negative Function")
	public void getportfoliodetailsAccountNegativeTest() {
		
		//context
		when(portfolioService.getPortfolioDetails(123)).thenReturn(portfoliofinal);
		
		//event
		PortfolioFinalDto portfolio=portfolioController.getPortfolios(1);
		
		//outcome
		assertNotEquals(portfolio,portfoliofinal);
}
	
	@Test
	@DisplayName("Place Order: Positive Function")
	public void getPlaceOrder() {
		// context
		when(portfolioService.placeOrdersByPortfolio(transactiondto)).thenReturn(true);
		// event
		ResponseEntity<String> result = portfolioController.placeOrderByPortfolio(transactiondto);
		// outcome
		assertEquals("Portfolio ordered successfully", result.getBody());

	}
	
	@Test
	@DisplayName("Place Order: Negative Function")
	public void getPlaceOrderTest() {
		// context
		when(portfolioService.placeOrdersByPortfolio(transactiondto)).thenReturn(false);
		// event
		ResponseEntity<String> result = portfolioController.placeOrderByPortfolio(transactiondto);
		// outcome
		assertEquals("Portfolio not ordered", result.getBody());

	}
	
}