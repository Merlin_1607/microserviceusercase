package com.hcl.PortfolioserviceTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import com.hcl.Portfolioservice.model.Portfolio;
import com.hcl.Portfolioservice.repository.PortfolioRepository;
import com.hcl.Portfolioservice.serviceimpl.PortfolioServiceImpl;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class PortfolioserviceApplicationServiceImplTest {

	@Mock
	PortfolioRepository portfolioRepo;

	@InjectMocks
	PortfolioServiceImpl portfolioService;
	static Portfolio portfolio;

	@BeforeAll
	public static void setup() {

		int min = 100;
		int max = 400;
		portfolio = new Portfolio();
		portfolio.setPortfolioName("Apple");
		double d = Math.random() * (max - min + 1) + min;
		portfolio.setCostofPurchase(d);

	}

	@Test
	@DisplayName("Portfolio details: Negative Function")
	public void getPortfolioDetailsNegativeTest() {

		// context
		when(portfolioRepo.findByPortfolioName("Apple")).thenReturn(portfolio);

		// event
		Double portfolios = portfolioService.getCostOfPurchase("HML");

		// outcome
		assertNotEquals(portfolio.getCostofPurchase(), portfolios);

	}

	

}
