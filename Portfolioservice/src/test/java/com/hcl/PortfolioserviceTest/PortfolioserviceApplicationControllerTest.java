package com.hcl.PortfolioserviceTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import com.hcl.Portfolioservice.controller.PortfolioServiceController;
import com.hcl.Portfolioservice.model.Portfolio;
import com.hcl.Portfolioservice.serviceimpl.PortfolioServiceImpl;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness=Strictness.LENIENT)
class PortfolioServiceApplicationControllerTest {

	@Mock
	PortfolioServiceImpl portfolioService;
	
	@InjectMocks
	PortfolioServiceController portfolioController;
	static Portfolio portfolio;
	static Double price;
	
	@BeforeAll
	public static void setup() {
	
		int min = 100;  
		int max = 400;  
				
		portfolio=new Portfolio();
        portfolio.setPortfolioName("Apple");        
        double d=Math.random() * (max - min + 1) + min ;
        portfolio.setCostofPurchase(d);
		

		
	

}
	@Test
	@DisplayName("Portfolio details: Negative Function")
	public void getPortfolioDetails() {
		
		//context
		when(portfolioService.getCostOfPurchase("Apple")).thenReturn(portfolio.getCostofPurchase());
		
		//event
		Double portfolios=portfolioController.getUnitCost("HML");
		
		//outcome
		assertNotEquals(portfolio.getCostofPurchase(),portfolios);
		
	}
	
	@Test
	@DisplayName("Portfolio details: Positive Function")
	public void getPortfolioDetailsTest() {
		
		//context
		when(portfolioService.getCostOfPurchase("Apple")).thenReturn(portfolio.getCostofPurchase());
		
		//event
		Double portfolios=portfolioController.getUnitCost("Apple");
		
		//outcome
		assertEquals(portfolio.getCostofPurchase(),portfolios);
		
	}
	
}
