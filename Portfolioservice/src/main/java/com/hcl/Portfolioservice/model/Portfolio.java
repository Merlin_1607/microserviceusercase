package com.hcl.Portfolioservice.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//Portfolio Entity

@Entity
public class Portfolio {
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private int portfolioId;
	private String portfolioName;
	private double costofPurchase;
	
	public Portfolio() {
		super();
	}
	public Portfolio(int portfolioId, String portfolioName, double costofPurchase) {
		super();
		this.portfolioId = portfolioId;
		this.portfolioName = portfolioName;
		this.costofPurchase = costofPurchase;
	}
	public int getPortfolioId() {
		return portfolioId;
	}
	public void setPortfolioId(int portfolioId) {
		this.portfolioId = portfolioId;
	}
	public String getPortfolioName() {
		return portfolioName;
	}
	public void setPortfolioName(String portfolioName) {
		this.portfolioName = portfolioName;
	}
	public double getCostofPurchase() {
		return costofPurchase;
	}
	@Override
	public String toString() {
		return "Portfolio [portfolioId=" + portfolioId + ", portfolioName=" + portfolioName + ", costofPurchase="
				+ costofPurchase + "]";
	}
	public void setCostofPurchase(double costofPurchase) {
		this.costofPurchase = costofPurchase;
	}

}
