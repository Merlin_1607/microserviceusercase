package com.hcl.Portfolioservice;
public class ApiStatusCode {

	public static final int STUDENT_NOT_FOUND = 600;
	public static final int INVALID_DATA = 400;
	public static final int PORTFOLIOID_NOT_FOUND = 500;
}