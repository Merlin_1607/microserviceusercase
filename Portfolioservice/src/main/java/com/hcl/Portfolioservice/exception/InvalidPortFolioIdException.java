package com.hcl.Portfolioservice.exception;

public class InvalidPortFolioIdException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	private String message;

	public InvalidPortFolioIdException(String message) {
		super(message);
		this.message = message;
	}

	public InvalidPortFolioIdException() {
		super();
	}


}
