package com.hcl.Portfolioservice.exception;



import java.time.LocalDateTime;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.hcl.Portfolioservice.ApiStatusCode;











@RestControllerAdvice //=  @ControllerAdvice + @ResponseBody
public class GlobalExceptionHandler {

	
	
	
	@ExceptionHandler(InvalidPortFolioIdException.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(InvalidPortFolioIdException ex) {
	ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatuscode(ApiStatusCode.PORTFOLIOID_NOT_FOUND);
		errorResponse.setDateTime(LocalDateTime.now());
		
		return new ResponseEntity<ErrorResponse>(errorResponse,HttpStatus.OK);
	}	
	
	
}