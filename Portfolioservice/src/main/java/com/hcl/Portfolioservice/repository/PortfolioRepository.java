package com.hcl.Portfolioservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.Portfolioservice.model.Portfolio;

public interface PortfolioRepository extends JpaRepository<Portfolio,Integer>{

	Portfolio findByPortfolioName(String portfolioName);

}
