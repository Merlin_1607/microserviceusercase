package com.hcl.Portfolioservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.Portfolioservice.service.PortfolioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
public class PortfolioServiceController {
	
	 // Configure loggers
    Logger logger = LoggerFactory.getLogger(PortfolioServiceController.class);
	
    @Autowired
	PortfolioService portfolioService;

	@GetMapping("/portfolio/{portfolioName}")
	public double getUnitCost(@PathVariable String portfolioName) {
		logger.trace("Get unit cost method is called");
		return portfolioService.getCostOfPurchase(portfolioName);

	}

}
