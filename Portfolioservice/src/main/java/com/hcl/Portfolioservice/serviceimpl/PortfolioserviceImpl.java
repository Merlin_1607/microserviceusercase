package com.hcl.Portfolioservice.serviceimpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.Portfolioservice.model.Portfolio;
import com.hcl.Portfolioservice.repository.PortfolioRepository;
import com.hcl.Portfolioservice.service.PortfolioService;

@Service
public class PortfolioServiceImpl implements PortfolioService {
	// Configure loggers
	Logger logger = LoggerFactory.getLogger(PortfolioServiceImpl.class);

	@Autowired
	PortfolioRepository portfolioRepo;

	@Override
	public double getCostOfPurchase(String portfolioName) {

		logger.trace("Calling get of cost purchase methos is called");
		int min = 100;
		int max = 400;
		Portfolio portfolio = new Portfolio();
		portfolio.setPortfolioName(portfolioName);
		double d = Math.random() * (max - min + 1) + min;
		logger.info("Unit cost is" + d);

		portfolio.setCostofPurchase(d);

		return portfolio.getCostofPurchase();

	}

}
