package com.hcl.userservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.userservice.dto.AccountDto;

import com.hcl.userservice.exception.InvalidCredentialsException;
import com.hcl.userservice.exception.NoUserIdFoundException;
import com.hcl.userservice.model.Account;
import com.hcl.userservice.model.User;
import com.hcl.userservice.repository.AccountRepository;
import com.hcl.userservice.repository.UserRepository;
import com.hcl.userservice.serviceimpl.UserServiceImpl;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness=Strictness.LENIENT)
public class UserServiceServieImplTest {
	
	

		@Mock
		UserRepository userRepo;
		
		@Mock
		AccountRepository accountRepo;
		
		@InjectMocks
		UserServiceImpl userServiceImpl;
		
	   
	    static User user;
		static User userPerst;
		static List<Account> accounts; 
		
		
		@BeforeAll
		public static void setUp() {
			
			
			user = new User();
			user.setUserName("Merlin");
			user.setPassword("mer123");
			User user=new User(1,"Merlin","mer123");
			Account account1=new Account(123,"Merlin","Savings",user);
			Account account2=new Account(1234,"Merlin","Current",user);
			accounts = new ArrayList<Account>();
			accounts.add(account1);
			accounts.add(account2);
			
			
		}
		
	
		
//		@Test
//		@DisplayName("authentication : positive scenario")
//		public void authenticationTest() {
//			//context
//			when(userRepo.findByUserNameAndPassword("Merlin", "mer123")).thenReturn(user);
//			
//			//event
//			ResponseEntity<String> result = userServiceImpl.authenticate("Merlin", "mer123");
//			
//			//outcome
//		    assertEquals("Login success", result.getBody());
//		    assertEquals(HttpStatus.OK, result.getStatusCode());
//		}
//		
//		@Test
//		@DisplayName("authentication : negative scenario")
//		public void authenticationTest1() {
//			//context
//			when(userRepo.findByUserNameAndPassword("Merlin", "mer123")).thenReturn(null);
//			
//			//event and outcome
//			assertThrows(InvalidCredentialsException.class, () ->userServiceImpl.authenticate("Merlin", "mer1234"));
//			
//		}
		

		@Test
		@DisplayName("Account details: Positive Function")
		public void getaccountdetails() {
			
			//context
			when(accountRepo.findByUserId(1)).thenReturn(accounts);
			
			//event
			List<AccountDto> account=userServiceImpl.getAccountDetails(1);
			
			//outcome
			assertEquals(accounts.size(),account.size());
			
		}
		

		@Test
		@DisplayName("Account details: NegativeFunction")
		public void getaccountdetailsnegative() {
			
			//context
			when(accountRepo.findByUserId(1)).thenReturn(accounts);
			
			
			
			//outcome
			assertThrows(NoUserIdFoundException.class,()->userServiceImpl.getAccountDetails(3));
			
		}
		
		
		

}

