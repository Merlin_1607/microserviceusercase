package com.hcl.userservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import com.hcl.userservice.controller.UserController;
import com.hcl.userservice.dto.AccountDto;
import com.hcl.userservice.dto.Credentials;

import com.hcl.userservice.exception.InvalidCredentialsException;
import com.hcl.userservice.exception.NoUserIdFoundException;
import com.hcl.userservice.model.Account;
import com.hcl.userservice.model.User;
import com.hcl.userservice.service.UserService;
import com.hcl.userservice.serviceimpl.UserServiceImpl;



@SpringBootTest
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness=Strictness.LENIENT)
class UserserviceControllerTest {

	@Mock
	UserServiceImpl userService;
	
	@InjectMocks
	UserController userController;
	
	
	
	static Credentials credentials;
	
	
    static List<AccountDto> accounts;
	
	@BeforeAll
	public static void setUp() {
		
	
		
		credentials = new Credentials();
		credentials.setUsername("Deeraj");
		credentials.setPassword("4567");
		User user=new User(1,"Merlin","mer123");
		AccountDto account1=new AccountDto("Merlin","Savings");
		AccountDto account2=new AccountDto("Merlin","Current");
		accounts=new ArrayList<AccountDto>();
		accounts.add(account1);
		accounts.add(account2);
		
		
	}
	
	
	
	
	
//	@Test
//	@DisplayName("Login Function: Positive Scenario")
//	public void loginTest() throws org.apache.http.auth.InvalidCredentialsException {
//		//context
//		when(userService.authenticate("Deeraj", "4567")).thenReturn(
//				new ResponseEntity<>("Login success", HttpStatus.OK));
//		
//		//event
//		ResponseEntity<String> result = userController.login(credentials);
//		
//		//outcome
//		assertEquals("Login success", result.getBody());
//	}
//	
//	@Test
//	@DisplayName("Login Function: Negative Scenario")
//	public void loginTest2() {
//		//context
//		when(userService.authenticate("Deeraj", "4567")).thenThrow(InvalidCredentialsException.class);
//		
//		//event
//		//outcome
//		assertThrows(InvalidCredentialsException.class, ()->userController.login(credentials));
//	}
	
	@Test
	@DisplayName("Account details: Positive Function")
	public void getaccountdetails() {
		
		//context
		when(userService.getAccountDetails(1)).thenReturn(accounts);
		
		//event
		List<AccountDto> account=userController.getaccounts(1);
		
		//outcome
		assertEquals(accounts.size(),account.size());
		
	}
	

	@Test
	@DisplayName("Account details: NegativeFunction")
	public void getaccountdetailsnegative() {
		
		//context
		when(userService.getAccountDetails(4)).thenReturn(accounts);
		
		List<AccountDto> account=userController.getaccounts(1);
		
		//outcome
		assertNotEquals(accounts.size(),account.size());
		
	}
	
}
