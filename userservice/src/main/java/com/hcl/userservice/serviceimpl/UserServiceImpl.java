package com.hcl.userservice.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.hcl.userservice.dto.AccountDto;
import com.hcl.userservice.exception.InvalidCredentialsException;
import com.hcl.userservice.exception.NoUserIdFoundException;
import com.hcl.userservice.model.User;
import com.hcl.userservice.repository.AccountRepository;
import com.hcl.userservice.repository.UserRepository;
import com.hcl.userservice.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService, UserDetailsService {
	
	Properties prop = new Properties();

	// Autowired Dependancies
	@Autowired
	UserRepository userrepo;
	@Autowired
	AccountRepository accountRepo;

	// Configure loggers
	Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	// Method for User Authentication

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userrepo.findByUserName(username);
		logger.info("Username : " + username);
		if (ObjectUtils.isEmpty(user)) {
			throw new InvalidCredentialsException("Invalid Credentials please try with corect one");
		}

		return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(),
				new ArrayList<>());
	}

	// Method for getting Number of Accounts of a User

	@Override
	public List<AccountDto> getAccountDetails(int userId) {

		List<AccountDto> accountdtoList = accountRepo.findByUserId(userId).stream().map(e -> {
			AccountDto accountdto = new AccountDto();
			accountdto.setAccountName(e.getAccountName());
			accountdto.setAccountType(e.getAccountType());
			return accountdto;
		}).collect(Collectors.toList());
		if (accountdtoList.isEmpty()) {
			throw new NoUserIdFoundException("No Account Exist with this UserId");
		}
		return accountdtoList;
	}

	

}
