package com.hcl.userservice.service.util;

import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;

import com.hcl.userservice.dto.UserRequestDto;
import com.hcl.userservice.model.User;


public class UserServiceUtils {

	public static User convertDtoToModel(UserRequestDto userRequestDto) {
		User user = new User();
		BeanUtils.copyProperties(userRequestDto, user);
		return user;
	}
}
