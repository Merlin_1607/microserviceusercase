package com.hcl.userservice.service;

import java.util.List;

import com.hcl.userservice.dto.AccountDto;

public interface UserService {
		
	public List<AccountDto> getAccountDetails(int userId);

}

