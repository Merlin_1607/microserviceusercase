package com.hcl.userservice;
public class ApiStatusCode {

	public static final int PRODUCT_NOT_FOUND = 600;
	public static final int INVALID_DATA = 400;
}