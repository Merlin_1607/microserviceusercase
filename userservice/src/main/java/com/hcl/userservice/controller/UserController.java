package com.hcl.userservice.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.http.auth.InvalidCredentialsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.userservice.dto.AccountDto;
import com.hcl.userservice.dto.Credentials;
import com.hcl.userservice.dto.JwtResponse;
import com.hcl.userservice.helper.JwtUtils;

import com.hcl.userservice.serviceimpl.UserServiceImpl;

@RestController
public class UserController {
	@Autowired
	UserServiceImpl userService;

	@Autowired
	JwtUtils jwtUtil;

	@Autowired(required = true)
	@Qualifier("authenticationManagerBean")
	AuthenticationManager authenticationManager;

	// Configure loggers
	Logger logger = LoggerFactory.getLogger(UserController.class);

	// Function for authentication by using spring security

	@RequestMapping(value = "/token", method = RequestMethod.POST)
	public ResponseEntity<?> generateToken(@RequestBody Credentials credentials) throws InvalidCredentialsException {
		logger.info("credentials " + credentials);
		try {
			this.authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(credentials.getUsername(), credentials.getPassword()));
		} catch (UsernameNotFoundException e) {
			throw new InvalidCredentialsException("Invalid Credentials please try with correct one");
		}
		UserDetails userDetails = this.userService.loadUserByUsername(credentials.getUsername());

		String token = jwtUtil.generateToken(userDetails);
		return ResponseEntity.ok(new JwtResponse(token));

	}

	// function for finding account details by userId
	@GetMapping("accounts/{userId}")
	public List<AccountDto> getaccounts(@PathVariable("userId") int userId) {
		logger.trace("Getting Account details method is called");
		return userService.getAccountDetails(userId);
	}

}
