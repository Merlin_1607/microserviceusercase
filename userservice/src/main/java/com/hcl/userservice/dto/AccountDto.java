package com.hcl.userservice.dto;

import javax.validation.constraints.NotEmpty;

public class AccountDto {
	@NotEmpty(message = "Account Name not be empty")
	private String accountName;
	@NotEmpty(message = "Account Type not be empty")
	private String accountType;
	

	
	public AccountDto() {
		super();
	}
	public AccountDto(@NotEmpty(message = "Account Name not be empty") String accountName,
			@NotEmpty(message = "Account Type not be empty") String accountType) {
		super();
		this.accountName = accountName;
		this.accountType = accountType;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String string) {
		this.accountName = string;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	@Override
	public String toString() {
		return "AccountDto [accountName=" + accountName + ", accountType=" + accountType + "]";
	}

}
