package com.hcl.userservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hcl.userservice.dto.Credentials;
import com.hcl.userservice.model.User;



@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	public User findByUserNameAndPassword(String username, String password);

	
	public User findByuserId(int userId);


	public User findByUserName(String username);
	
}
