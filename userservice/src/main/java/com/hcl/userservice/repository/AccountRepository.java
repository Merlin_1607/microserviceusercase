package com.hcl.userservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hcl.userservice.dto.AccountDto;
import com.hcl.userservice.model.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {
	@Query(value = "SELECT * FROM Account a WHERE a.user_id = ?1", nativeQuery = true)

	public List<Account> findByUserId(int userId);

}
